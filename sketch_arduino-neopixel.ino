#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
 
#define PIN            5   //Arduinoで使うピン
#define NUMPIXELS      1   //LEDの数。
 
//ライブラリのセットアップ
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
 
void setup() {
  pixels.begin(); // ライブラリ使用開始
  Serial.begin(115200);
  pinMode(0, INPUT);
}

uint8_t bright = 5;
int prevVal = HIGH;
void loop() {
  if (digitalRead(0) == LOW && prevVal == HIGH) {
    bright += 5;
  }
  prevVal = digitalRead(0);

  //0番目のLEDのRGBを指定
  float phase = millis()/400.0;
  uint8_t r = 127 * (1 + sin(phase));
  uint8_t g = 127 * (1 + sin(phase + 6.28/3));
  uint8_t b = 127 * (1 + sin(phase + 12.56/3));
  pixels.setPixelColor(0,pixels.Color(g,b,r));
  pixels.setBrightness(bright);
  yield();

  //出力　
  pixels.show(); 
}
